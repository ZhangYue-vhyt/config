# Created by newuser for 5.7.1
# source /usr/share/zsh/scripts/zplug/init.zsh	
# source ~/.zplug/init.zsh


##############################
# Zplug
##############################

# Clear packages
zplug clear

# Zplug plugins
# zplug "zsh-users/zsh-completions"
# zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-syntax-highlighting", defer:3
zplug "zsh-users/zsh-history-substring-search"
zplug 'plugins/git', from:oh-my-zsh
zplug 'plugins/vi-mode', from:oh-my-zsh
zplug 'plugins/docker', from:oh-my-zsh
zplug 'lib/completion', from:oh-my-zsh
zplug 'plugins/docker-compose', from:oh-my-zsh
setopt prompt_subst # Make sure prompt is able to be generated properly.
zplug "caiogondim/bullet-train.zsh", use:bullet-train.zsh-theme, defer:3 # defer until other plugins like oh-my-zsh is loaded

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load --verbose

# Alias
alias docker-compose=~/.local/bin/docker-compose
