# Requirements of Manjaro i3wm
- firefox
- terminator
- zsh
- zplug

# How to use
```sh
cp .zshrc ~/.zshrc
cp terminator ~/.config/
cp .i3 ~/.i3/
```